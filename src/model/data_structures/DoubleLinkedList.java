package model.data_structures;

import java.util.Iterator;

public class DoubleLinkedList<T> implements IList
{
	private Node primero = null;
	private Node ultimo = null;
	private Node actual = primero;

	private class Node 
	{
		T elemento;
		Node siguiente;
		Node anterior;
	}

	public void reiniciar()
	{
		actual = primero;
	}

	public Integer getSize()
	{
		Integer size = 0;
		if(primero == null)
		{
			size = 0;
		}
		else
		{
			while(hasNext())
			{
				size++;
				next();
			}
		}
		reiniciar();
		return size;
	}

	public void add(Object agregar)
	{
		Node agregado = new Node();
		agregado.elemento = (T) agregar;
		if(primero == null)
		{
			primero = agregado;
			ultimo = agregado;
		}
		else
		{
			Node oldFirst = primero;
			primero = agregado;
			oldFirst.anterior = primero;
			primero.siguiente = oldFirst;
			primero.anterior = null;
		}
	}

	public void addAtEnd(Object agregar)
	{
		Node agregado = new Node();
		agregado.elemento = (T) agregar;
		if(primero == null)
		{
			primero = agregado;
			ultimo = agregado;
		}
		else
		{
			Node oldLast = ultimo;
			ultimo = agregado;
			oldLast.siguiente = ultimo;
			ultimo.anterior = oldLast;
			ultimo.siguiente = null;
		}
	}

	public void addAtK(Object agregar, int posicion)
	{
		Node agregado = new Node();
		agregado.elemento = (T) agregar;
		if(primero == null)
		{
			primero = agregado;
		}
		else
		{
			Integer cont = 0;
			while(hasNext() && cont <= posicion)
			{
				next();
				if(cont == posicion)
				{
					if(posicion == 0)
					{
						agregado.siguiente = primero;
						primero.anterior = agregado;
						primero = agregado;
					}
					else
					{
						agregado.anterior = actual.anterior;
						agregado.siguiente = actual;
						actual.anterior = agregado;
					}
				}
				else
				{
					cont++;
				}
			}
		}
		reiniciar();
	}

	public void delete(Object eliminar)
	{
		boolean encontrado = false;
		while(hasNext() && !encontrado)
		{
			if(actual.elemento.equals(eliminar))
			{
				if(actual.equals(primero) && actual.equals(ultimo))
				{
					primero = null;
					ultimo = null;
				}	
				else if(actual.equals(primero))
				{
					actual.siguiente.anterior = actual.anterior;
					primero = actual.siguiente;
				}
				else if(actual.equals(ultimo))
				{
					actual.anterior.siguiente = actual.siguiente;
					ultimo = actual.anterior;
				}
				else
				{
					actual.anterior.siguiente = actual.siguiente;
					actual.siguiente.anterior = actual.anterior;
				}
				encontrado = true;
			}
			next();
		}
		reiniciar();
	}

	public void deleteAtK(int posicion)
	{
		int cont = 0;
		while(hasNext() && cont <= posicion)
		{
			if(cont == posicion)
			{
				if(actual.equals(primero) && actual.equals(ultimo))
				{
					primero = null;
					ultimo = null;
				}
				else if(actual.equals(primero))
				{
					actual.siguiente.anterior = actual.anterior;
					primero = actual.siguiente;
				}
				else if(actual.equals(ultimo))
				{
					actual.anterior.siguiente = actual.siguiente;
					ultimo = actual.anterior;
				}
				else
				{
					actual.anterior.siguiente = actual.siguiente;
					actual.siguiente.anterior = actual.anterior;
				}
				actual.anterior.siguiente = actual.siguiente;
				actual.siguiente.anterior = actual.anterior;
			}
			else
			{
				cont++;
				next();
			}
		}
		reiniciar();
	}

	public Object getElement(int posicion)
	{
		int cont = 0;
		T rta = null;
		while(hasNext() && cont <= posicion)
		{
			if(cont == posicion)
			{
				rta = actual.elemento;
			}
			else
			{
				cont++;
				next();
			}
		}
		reiniciar();
		return rta;
	}

	public Iterator<T> iterator()
	{
		return null;
	}

	public T next() 
	{
		T current = actual.elemento;
		actual = actual.siguiente;
		return current;
	}

	public boolean hasNext()
	{
		return actual.siguiente != null;
	}

	public T previous()
	{
		T current = actual.elemento;
		actual = actual.anterior;
		return current;
	}

	public T getCurrentElement()
	{
		return actual.elemento;
	}
}