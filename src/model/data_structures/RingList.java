package model.data_structures;

import java.util.Iterator;


public class RingList<T> implements IList<T>
{
	private Node primero = null;
	private Node actual = primero;
	private Node ultimo = null;


	public Iterator<T> iterator() 
	{

		return null;
	}
	private class Node 
	{
		T elemento;
		Node siguiente;
		Node anterior;

	}

	public Integer  getSize()
	{	  
		if (primero ==null)
		{
			return 0;
		} 
		int x =0;
		while(hasNext())
		{ 
			next();
			x++;
		}
		reiniciar();
		return x;	

	}


	public boolean hasNext() 
	{

		return actual.siguiente!= primero.anterior;
	}


	public void add(T agregar) 
	{   
		Node agregado = new Node();
		agregado.elemento = agregar;

		if (primero == null)
		{ 
			primero = agregado;

		}

		else
		{
			Node oldfirst = primero;
			primero = agregado;
			agregado.anterior = ultimo;
			agregado.siguiente = oldfirst;
			ultimo.siguiente = agregado;
		}


	}

	public void addAtEnd(T agregar)
	{
		Node nuevo = new Node();

		nuevo.elemento = agregar;


		if (primero == null) 
		{
			primero = nuevo;
		} 
		else{

			ultimo.siguiente = nuevo;

			nuevo.anterior = ultimo;

			nuevo.siguiente = primero;

			primero.anterior = nuevo;		

			ultimo = nuevo;
		}
	}

	public void addAtK(T agregar, int posicion)
	{
		Node agregado = new Node();
		agregado.elemento = (T) agregar;
		if(primero == null)
		{
			primero = agregado;
		}

		else
		{
			int cont = 0;

			while (hasNext()&& cont <= posicion)
			{
				next();
				if (cont == posicion)
				{
					if (cont == posicion)
					{
						agregado.siguiente = primero;
						primero.anterior = agregado;
						primero = agregado;
					}
					else
					{
						agregado.anterior= actual.anterior;
						agregado.siguiente = actual;
						actual.anterior = agregado;
					}
				}
				else
				{
					cont++;
				}
			}
		}
		reiniciar();

	}

	public T getElement(int posicion)
	{
		int cont =0;
		T rta = null;
		while(hasNext()&&cont <=posicion)
		{
			if(cont == posicion)

			{
				rta=actual.elemento;
			}
		}
		reiniciar();
		return rta;		
	}

	public T getCurrentElement()
	{
		return actual.elemento;
	}

	public void delete(T eliminar)
	{
		boolean encontrado = false;
		while(hasNext()&&!encontrado)
		{
			if(actual.elemento.equals(eliminar))
			{
				if(actual.equals(primero)&&actual.equals(ultimo))

				{
					primero =null;
					ultimo =null;
				}

				else if(actual.equals(primero))
				{
					actual.siguiente.anterior = actual.anterior;
					primero = actual.siguiente;
				}

				else if(actual.equals(ultimo))
				{
					actual.anterior.siguiente = actual.anterior;
					ultimo = actual.anterior;

				}
				else
				{
					actual.anterior.siguiente= actual.siguiente;
					actual.siguiente.anterior = actual.anterior;
				}

				encontrado=true;
			}
			next();

		}
		reiniciar();
	}

		public void deleteAtK(int posicion)
		{
			int cont = 0;
			while(hasNext() && cont <= posicion)
			{
				if(cont == posicion)
				{
					if(actual.equals(primero) && actual.equals(ultimo))
					{
						primero = null;
						ultimo = null;
					}
					else if(actual.equals(primero))
					{
						actual.siguiente.anterior = actual.anterior;
						primero = actual.siguiente;
					}
					else if(actual.equals(ultimo))
					{
						actual.anterior.siguiente = actual.siguiente;
						ultimo = actual.anterior;
					}
					else
					{
						actual.anterior.siguiente = actual.siguiente;
						actual.siguiente.anterior = actual.anterior;
					}
					actual.anterior.siguiente = actual.siguiente;
					actual.siguiente.anterior = actual.anterior;
				}
				else
				{
					cont++;
					next();
				}
			}
			reiniciar();
		}


		public T previous()
		{
			return (T) actual.anterior;
		}


		public T next() 
		{
			T current = (T) actual;
			actual = actual.siguiente;
			return current;
		}
		public void reiniciar()
		{
			actual = primero;
		}


	}
